# Returns a list with any element from the input collection removed if it appears more than once.
# Example: [ 1, 1, 3 ] returns [ 3 ] because 1 appears more than once.
def remove_duplicates(list):
    newlist = []
    newlist1 = []
    for i in list:
        if i not in newlist:
            newlist.append(i)
        else:
            newlist1.append(i)
    for i in newlist1:
        if i in newlist1 and i in newlist:
            newlist.remove(i)
        else:
            continue
    return newlist

def test_remove_duplicates():
    assert remove_duplicates([1, 1, 1, 2, 3, 3, 4]) == [2, 4]
    assert remove_duplicates([7,7,7,7,7,-7,-7,-7,8]) == [8]
    assert remove_duplicates([10, 20, 30, 40, 50]) == [10, 20, 30, 40, 50]
    assert remove_duplicates([(5+5), 10, 15]) == [15]