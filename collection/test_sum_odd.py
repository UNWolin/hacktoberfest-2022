# Returns the sum of all odd numbers within the provided collection.
def sum_all_odd(collection):
    numList = list(collection)
    sum = 0
    count = 0

    for i in numList:
        if (i % 2) != 0:
            sum += i
            count += 1
        else:
            count += 1

    return sum


def test_sum_all_odd():
    assert sum_all_odd([1, 4, 4, 6, 7, 9, 2, 0]) == 17


def test_sum_all_odd2():
    assert sum_all_odd([33, 64, 20, 11, 57, 9]) == 110


def test_sum_all_odd3():
    assert sum_all_odd([64, 393, 27, 58, 99, 2]) == 519


def test_sum_all_odd4():
    assert sum_all_odd([7, 49, 81, 126, 182, 951, 4]) == 1088
