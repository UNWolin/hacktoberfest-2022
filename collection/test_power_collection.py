# Given a collection of numbers, return a collection of the same size consisting of the square of each element.
# Example: func([1, 2, 3]) is [1, 4, 9], because each element is squared and present at the same index.
# BONUS CHALLENGE: add an argument to specify the power each number is raised to!
def to_power_collection(numbers):
    data = [number ** 2 for number in numbers]

    return data


def test_to_power_collection():
    assert to_power_collection([1, 2, 3, 4, 5]) == [1, 4, 9, 16, 25]
    assert to_power_collection([1, 2, 3, 4, 5, 6]) == [1, 4, 9, 16, 25, 36]
    assert to_power_collection([1, 2, 3, 4, 5, 6, 7]) == [1, 4, 9, 16, 25, 36, 49]
    assert to_power_collection([1, 2, 3, 4, 5, 6, 7, 8]) == [1, 4, 9, 16, 25, 36, 49, 64]
    assert to_power_collection([1, 2, 3, 4, 5, 6, 7, 8, 9]) == [1, 4, 9, 16, 25, 36, 49, 64, 81]
    assert to_power_collection([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]) == [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]
    assert to_power_collection([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]) == [1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121]

