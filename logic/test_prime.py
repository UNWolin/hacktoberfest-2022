# Returns true if the input value is prime.
def is_prime(value):
    if value > 1:
        newvalue = value/2
        if newvalue.is_integer():
            print("The number is not a prime number.")
            return False
        else:
            print("The number is a prime number.")
            return True
    else:
        print("Use another prime number algorithm!")


def test_is_prime():
    assert is_prime(94847)
    assert not is_prime(10000)
    assert not is_prime(5010)
    assert is_prime(4997)
    assert not is_prime(-5)

