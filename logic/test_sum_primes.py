# Deklan Glaser
# Return the sum of all primes under n.
from multiprocessing import Pool
import math
global x
x = 100
def prime(num):
    a = 2
    while a <= math.sqrt(num):
        if num % a < 1:
            return False
        a = a + 1
    return num > 1
def sum_primes_under(n):
    # 5 pool = 0.11615109443664551 with 100000 numbers
    # 10 pool = 0.13418960571289062 with 100000 numbers
    # 20 pool = 0.18252897262573242 with 100000 numbers

    # with 10000000 numbers
    # 5 pool = 37.74760818481445
    # 10 pool = 28.437782049179077
    # 15 pool = 27.640254974365234
    # 20 pool = 27.432414054870605
    # Diminishing return when you exceed core count of machine
    with Pool(15) as p:
        li = []
        total = 0
        for i in range(n):
            li.append(i)
        primeList = p.map(prime, li)

    for i in range(len(li)):
        if primeList[i] == True:
            total += li[i]
    print(total)
    return total


def test_is_prime():
    assert sum_primes_under(100000) == 454396537

    for i in range(x):
        val = sum_primes_under(i)
        print(val)
        exec(f"def test_prime_{i}(): assert {val} == {val}")
