# A string is a palindrome if it is the same backwards as it is forwards.
# aba is a palindrome because it is still aba if you flip it around.
# This method will return True if the input string is a palindrome.
def is_palindrome(string):
    stringList = []
    for i in string:
        stringList.append(i)
    backwardsStringList = []
    for i in range(len(stringList)):
        print(i)
        backwardsStringList.append(stringList[(len(stringList) - (i+1))])
    if stringList == backwardsStringList:
        return True
    else:
        return False


def test_is_palindrome():
    assert is_palindrome("racecar")
    assert is_palindrome("mom")
