# Returns the result of x / y.
def divide_numbers(x, y):
    result=x/y
    return result



def test_divide_numbers():
    assert divide_numbers(10, 5) == 2

    assert divide_numbers(20, 5) == 4

    assert divide_numbers(100, 5) == 20

    assert divide_numbers(40, 10) == 4

    assert divide_numbers(50, 5) == 10

    assert divide_numbers(50, 10) == 5
