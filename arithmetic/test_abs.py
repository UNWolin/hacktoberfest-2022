# Returns the absolute value of x.
# Note: you can't use Python's built-in absolute value function!
def compute_absolute_value(x):
    return 0


def test_abs():
    assert compute_absolute_value(-5) == 5


def test_abs2():
    assert compute_absolute_value(-5000) == 5000


def test_abs3():
    assert compute_absolute_value(100) == 100

def test_abs4():
    assert compute_absolute_value(7) == 7