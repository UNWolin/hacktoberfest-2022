# Returns the result of adding x and y together.

def add_numbers(x, y):
    add_numbers = x + y
    return add_numbers

def test_add_numbers():
    assert add_numbers(1, 2) == 3


def test_add_numbers2():
    assert add_numbers(1000, 20000) == 21000


def test_add_numbers3():
    assert add_numbers(1, -9) == -8

def test_add_numbers4():
    assert add_numbers(26, -9) == 17