# Returns the result of x mod y.
# Modulus (x % y) returns the remainder of <x / y>.
def mod_numbers(x, y):
    mod = x % y
    return mod


def test_add_numbers():
    assert mod_numbers(6, 4) == 2
    assert mod_numbers(2, 9) == 2
    assert mod_numbers(5, 9) == 5
    assert mod_numbers(2.5, 9) == 2.5
