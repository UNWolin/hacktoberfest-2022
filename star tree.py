# Create and return a Christmas Tree out of # characters, based off the number of levels specified.
# christmas_tree(3) will return:
#            __#__
#            _###_
#            #####
# (without the underscores, which represent white space)

def christmas_tree():
    string = ""
    tree_height = 3
    spaces = tree_height - 1
    hashes = 1

    while tree_height != 0:
        for i in range(spaces):
            string+=' '


        for i in range(hashes):
            string+='*'


        for i in range(spaces):
            string+=' '
        tree_height -= 1
        spaces -= 1
        hashes += 2

    return string

def test_christmas_tree():
    assert christmas_tree() == ("  *  "
                                 " *** "
                                 "*****")
