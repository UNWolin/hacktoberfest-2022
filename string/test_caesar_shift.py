# Return a string with all characters shifted up or down in the alphabet by the amount specified in <by>.
def caesar_shift(value, by):
    alphabet = "abcdefghijklmnopqrstuvwxyz"

    def encrypt(target, shift):
        for index in range(len(alphabet)):
            if alphabet[index] == target:
                x = index + shift
                y = x % len(alphabet)
                return (alphabet[y])

    encrypted_string = ''
    for x in value:
        if x == ' ':
            encrypted_string += ' '
        else:
            encrypted_string += encrypt(x, by)

    return(encrypted_string)


def test_caesar_shift():
    assert caesar_shift("abc", 1) == "bcd"
    assert caesar_shift("abc", -1) == "zab"
    assert caesar_shift("abc", 2) == "cde"


