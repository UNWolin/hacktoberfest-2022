# Returns the input string in lowercase.
def to_lowercase(string):
    return string.lower()


def test_to_lowercase():
    assert to_lowercase("I LOVE PIZZA!") == "i love pizza!"

    assert to_lowercase("SOMETHING") == "something"

    assert to_lowercase("SOME") == "some"

    assert to_lowercase("SENTENCE") == "sentence"

    assert to_lowercase("WORD") == "word"

    assert to_lowercase("ANSEL") == "ansel"