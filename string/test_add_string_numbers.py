# Given an input collection of strings that can be parsed as integers, returns the resulting sum of all input values
# when converted to an integer, as a string.
def add_string_numbers(collection):
    for i in range(len(collection)):
        collection[i] = int(collection[i])
    p = sum(collection)
    return str(p)


def test_add_string_numbers():
    assert add_string_numbers(["1", "2"]) == "3"
    assert add_string_numbers(["100", "200", "50"]) == "350"
    assert add_string_numbers(["100", "200"]) == "300"
    assert add_string_numbers(["103", "200"]) == "303"
    assert add_string_numbers(["102", "200"]) == "302"
    assert add_string_numbers(["101", "200"]) == "301"

