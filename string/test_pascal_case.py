# Converts and returns the input to PascalCase.
# In PascalCase, the first character & characters after spaces are upper-cased.
# All other characters are lower-cased. Spaces are removed.


#convert
def to_pascal_case(string):
    stringList = string.split(' ')
    capitalizedList = [p.capitalize() for p in stringList]
    capString = "".join(capitalizedList)
    print(capString)
    test_pascal_case(capString)

def test_pascal_case():
    assert True
